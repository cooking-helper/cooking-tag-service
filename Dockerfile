FROM openjdk:8 AS build_image
ENV APP_HOME=/root/dev/cooking-tag-service/
RUN mkdir -p $APP_HOME/src/main/java
WORKDIR $APP_HOME
COPY build.gradle integration-test.gradle gradlew gradlew.bat $APP_HOME
COPY gradle $APP_HOME/gradle
# download dependencies
RUN chmod 777 gradlew
RUN ./gradlew build -x test -x integrationTest || return 0
COPY . .
RUN ./gradlew build -x test -x integrationTest


FROM openjdk:8-jre
WORKDIR /root/
COPY --from=build_image /root/dev/cooking-tag-service/build/libs/cooking-tag-service*.jar ./cooking-tag-service.jar
COPY src/main/resources/docker_config.properties /root/config.properties
EXPOSE 8080
CMD ["java","-jar","cooking-tag-service.jar","--spring.config.location=file:///root/config.properties"]