package com.aott.cooking.tag.rest.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/status")
class ServiceStatusController @Autowired constructor(
) {
    @GetMapping
    fun isRunning(): Boolean {
        return true
    }
}