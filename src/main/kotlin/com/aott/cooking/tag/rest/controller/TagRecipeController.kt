package com.aott.cooking.tag.rest.controller


import com.aott.cooking.tag.database.mongo.queries.MongoRecipesQueries
import com.aott.cooking.tag.model.Recipe
import com.aott.cooking.tag.model.TagEnum
import com.aott.cooking.tag.tagger.ITagStrategy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/recipe")
class TagRecipeController @Autowired constructor(
        private val recipesQueries: MongoRecipesQueries,
        private val tagStrategy: ITagStrategy
) {
    @GetMapping("/{id}/tag")
    fun getRecipeTag(@PathVariable id: String): List<TagEnum> {
        val recipe: Recipe = recipesQueries.findRecipeById(id)
        if (recipe.tags == null) {
            return tagStrategy.getRecipeTag(recipe)
        } else {
            return recipe.tags!!
        }
    }

    @PutMapping(value = "/{id}/tag")
    fun updateRecipeTag(@PathVariable id: String, @RequestBody tags: List<TagEnum>) {
        recipesQueries.setTagsForId(id!!.toString(), tags)
    }
}