package com.aott.cooking.importer.rest.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [UnknownWebSiteException::class])
    protected fun handleNotImplementedError(
            ex: UnknownWebSiteException, request: WebRequest): ResponseEntity<ApiError> {
        val apiError = ApiError(ex.message, ex)
        return ResponseEntity(apiError, HttpStatus.NOT_IMPLEMENTED)
    }

    @ExceptionHandler(value = [Exception::class])
    protected fun handleInternalServerError(
            ex: Exception, request: WebRequest): ResponseEntity<ApiError> {
        val apiError = ApiError(ex.message, ex)
        return ResponseEntity(apiError, HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
