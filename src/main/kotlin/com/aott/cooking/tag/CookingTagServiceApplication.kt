package com.aott.cooking.tag

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CookingTagServiceApplication

fun main(args: Array<String>) {
    runApplication<CookingTagServiceApplication>(*args)
}
