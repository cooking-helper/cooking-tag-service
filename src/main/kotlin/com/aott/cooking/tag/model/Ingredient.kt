package com.aott.cooking.tag.model

data class Ingredient(val name: String, val quantity: Int, val unit: String)
