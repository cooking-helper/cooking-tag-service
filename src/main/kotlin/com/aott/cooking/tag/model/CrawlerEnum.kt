package com.aott.cooking.tag.model

enum class CrawlerEnum(val id: String) {
    NONE("none"), MARMITON("marmiton"), POTAGER_CITY("potagercity")
}