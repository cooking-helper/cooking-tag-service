package com.aott.cooking.tag.model

import java.util.*
import kotlin.collections.ArrayList

data class Recipe(
        var id: String? = null,
        var updateDate: Date,
        var url: String,
        var source: CrawlerEnum = CrawlerEnum.NONE,
        var title: String = "",
        var totalTime: Int = 0,
        var prepareTime: Int = 0,
        var cookingTime: Int = 0,
        var difficulty: Int = 0,
        var cost: Int = 0,
        var numberOfPiece: Int = 0,
        var ingredients: List<Ingredient> = ArrayList(),
        var instructions: String = "",
        var tags: List<TagEnum>? = null) {
    constructor() : this(null, Date(),
            "", CrawlerEnum.NONE, "", 0, 0, 0, 0, 0, 0, ArrayList(), "", null
    )
}
