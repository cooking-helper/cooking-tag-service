package com.aott.cooking.tag.model

enum class TagEnum(val id: String) {
    WINTER("winter"), SUMMER("summer"),
    LIGHT("light"), CHEAP("cheap"), QUICK("quick"), EASY("easy"),
    DESSERT("dessert"), STARTER("starter"), MAIN_COURSE("main course"),
    BREAKFAST("breakfast"), LUNCH("lunch"), DINNER("dinner")
}