package com.aott.cooking.tag.database.mongo.util

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.data.authentication.UserCredentials


@Configuration
@PropertySource("\${spring.config.location:}")
@ConfigurationProperties(prefix = "tag.mongo")
class MongoConfig {
    private var server = "localhost"
    private var port = 27017
    private var authentication = false
    private var user = ""
    private var password = ""
    private var database = "cooking"

    fun setServer(serverVal: String) {
        server = serverVal
    }

    fun setPort(portVal: Int) {
        port = portVal
    }

    fun setAuthentication(authenticationVal: Boolean) {
        authentication = authenticationVal
    }

    fun setUser(userVal: String) {
        user = userVal
    }

    fun setPassword(passwordVal: String) {
        password = passwordVal
    }

    fun setDatabase(databaseVal: String) {
        database = databaseVal
    }

    fun getHost(): String {
        return server
    }

    fun getPort(): Int {
        return port
    }

    fun getCredentials(): UserCredentials {
        return UserCredentials(user, password)
    }

    fun useAuthentication(): Boolean {
        return authentication
    }

    fun getDatabaseName(): String {
        return database
    }
}