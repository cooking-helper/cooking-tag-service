package com.aott.cooking.tag.database.mongo.model

import com.aott.cooking.tag.model.CrawlerEnum
import com.aott.cooking.tag.model.Ingredient
import com.aott.cooking.tag.model.TagEnum
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import java.util.*

data class MongoRecipe(
        @Id
        var _id: ObjectId? = null,
        var updateDate: Date = Date(),
        var url: String,
        var source: CrawlerEnum = CrawlerEnum.NONE,
        var title: String = "",
        var totalTime: Int = 0,
        var prepareTime: Int = 0,
        var cookingTime: Int = 0,
        var difficulty: Int = 0,
        var cost: Int = 0,
        var numberOfPiece: Int = 0,
        var ingredients: List<Ingredient> = ArrayList(),
        var instructions: String = "",
        var tags: List<TagEnum>? = null
) {
    constructor() : this(null, Date(),
            "", CrawlerEnum.NONE, "", 0, 0, 0, 0, 0, 0, ArrayList(), "", null
    )
}