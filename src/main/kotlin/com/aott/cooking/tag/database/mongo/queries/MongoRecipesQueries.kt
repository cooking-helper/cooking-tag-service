package com.aott.cooking.tag.database.mongo.queries

import com.aott.cooking.tag.database.mongo.model.MongoRecipe
import com.aott.cooking.tag.database.mongo.model.convertToDomain
import com.aott.cooking.tag.database.mongo.util.MongoConnection
import com.aott.cooking.tag.model.Recipe
import com.aott.cooking.tag.model.TagEnum
import org.bson.types.ObjectId
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection
import org.litote.kmongo.updateOneById
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class MongoRecipesQueries @Autowired constructor(
        private val mongo: MongoConnection
) {
    fun findRecipeById(id: String): Recipe {
        val mongoRecipe = mongo.database.getCollection<MongoRecipe>().findOne("{_id:ObjectId(\"$id\")}")
        return convertToDomain(mongoRecipe!!)
    }

    fun setTagsForId(id: String, tags: List<TagEnum>) {
        mongo.database.getCollection<MongoRecipe>().updateOneById(ObjectId(id), "{\$set:{tags:" + tags.toString() + "}}")
    }

    fun findAllrecipes(): List<Recipe> {
        val allRecipes = mutableListOf<Recipe>()
        val mongoRecipesIterator = mongo.database.getCollection<MongoRecipe>().find()
        for (mongoRecipe in mongoRecipesIterator) {
            allRecipes.add(convertToDomain(mongoRecipe))
        }
        return allRecipes
    }
}