package com.aott.cooking.tag.database.mongo.model

import com.aott.cooking.tag.model.Recipe
import org.bson.types.ObjectId
import org.modelmapper.ModelMapper

private val modelMapper = ModelMapper()

fun convertToDomain(recipeMongoModel: MongoRecipe): Recipe {
    val recipeDomainModel = modelMapper.map(recipeMongoModel, Recipe::class.java)
    if (recipeMongoModel._id == null) {
        recipeDomainModel.id = null
    } else {
        recipeDomainModel.id = recipeMongoModel._id.toString()
    }
    return recipeDomainModel
}

fun convertToMongo(recipeDomainModel: Recipe): MongoRecipe {
    val recipeMongoModel = modelMapper.map(recipeDomainModel, MongoRecipe::class.java)
    if (recipeDomainModel.id == null) {
        recipeMongoModel._id = null
    } else {
        recipeMongoModel._id = ObjectId(recipeDomainModel.id)
    }
    return recipeMongoModel
}