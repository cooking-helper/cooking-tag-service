package com.aott.cooking.tag.tagger

import com.aott.cooking.tag.model.Recipe
import com.aott.cooking.tag.model.TagEnum
import org.springframework.stereotype.Service

@Service
class HumanRuleTagStrategy : ITagStrategy {
    val fatCoefficients: Map<String, Int> = mutableMapOf("beurre" to 5, "sucre" to 10, "canard" to 3, "saumon" to 2,
            "boeuf" to 3, "agneau" to 3, "mouton" to 3, "rôti de porc " to 3, "cotelette de porc" to 3,
            "huile" to 5, "lait" to 2, "oeuf" to 5, "crème" to 5, "yaourt" to 2, "boursin" to 3, "parmesan" to 3,
            "comté" to 3, "emmental" to 3, "beaufort" to 3, "brie" to 3, "chèvre" to 2, "coco" to 3)

    val dessertIngredients: List<String> = listOf("sucre", "oeuf", "lait", "farine", "pomme", "poire", "pêche", "chocolat", "banane", "vanille", "beurre", "levure")
    val winterIngredients: List<String> = listOf("patate", "pomme de terre", "fromage")
    override fun getRecipeTag(recipe: Recipe): List<TagEnum> {
        val findedTags: MutableList<TagEnum> = mutableListOf()
        if (recipe.prepareTime <= 30 && recipe.totalTime <= 40) {
            findedTags.add(TagEnum.QUICK)
        }
        if (recipe.cost <= 2) {
            findedTags.add(TagEnum.CHEAP)
        }
        if (recipe.difficulty <= 2) {
            findedTags.add(TagEnum.EASY)
        }

        var fatScore = 0;
        var dessertScore = 0
        var winterScore = 0
        for (ingredient in recipe.ingredients) {
            for (fatCoefficient in fatCoefficients.entries) {
                if (ingredient.name.contains(fatCoefficient.key)) {
                    fatScore += fatCoefficient.value
                }
            }
            for (dessertIngredient in dessertIngredients) {
                if (ingredient.name.contains(dessertIngredient)) {
                    dessertScore += 1
                }
            }

            for (winterIngredient in winterIngredients) {
                if (ingredient.name.contains(winterIngredient)) {
                    winterScore += 1
                }
            }
        }

        if (fatScore < 10) {
            findedTags.add(TagEnum.LIGHT)
        }

        if (dessertScore > 3) {
            findedTags.add(TagEnum.DESSERT)
        } else {
            findedTags.add(TagEnum.MAIN_COURSE)
        }

        if (findedTags.contains(TagEnum.LIGHT) || findedTags.contains(TagEnum.QUICK)) {
            findedTags.add(TagEnum.DINNER)
        }
        findedTags.add(TagEnum.LUNCH)


        if (recipe.cookingTime == 0 || winterScore < 2) {
            findedTags.add(TagEnum.SUMMER)
        } else {
            findedTags.add(TagEnum.WINTER)
        }

        return findedTags
    }

}