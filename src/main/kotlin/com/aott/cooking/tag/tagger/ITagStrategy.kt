package com.aott.cooking.tag.tagger

import com.aott.cooking.tag.model.Recipe
import com.aott.cooking.tag.model.TagEnum

interface ITagStrategy {
    fun getRecipeTag(recipe: Recipe): List<TagEnum>
}