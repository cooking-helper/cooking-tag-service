package com.aott.cooking.search

import com.aott.cooking.search.util.tarteCarotteOignons
import com.aott.cooking.tag.CookingTagServiceApplication
import com.aott.cooking.tag.database.mongo.model.MongoRecipe
import com.aott.cooking.tag.database.mongo.model.convertToDomain
import com.aott.cooking.tag.database.mongo.model.convertToMongo
import com.aott.cooking.tag.database.mongo.util.MongoConfig
import com.aott.cooking.tag.model.Recipe
import com.aott.cooking.tag.model.TagEnum
import com.aott.cooking.tag.rest.controller.TagRecipeController
import com.mongodb.MongoClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.litote.kmongo.KMongo
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForObject
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [CookingTagServiceApplication::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = ["classpath:test.properties"])
class TagServiceTests {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    private val controller: TagRecipeController? = null

    @Autowired
    private val mongoConfig: MongoConfig? = null

    private var mongoClient: MongoClient? = null

    @Before
    fun cleanDatabase() {
        if (mongoConfig != null) {
            mongoClient = KMongo.createClient(mongoConfig.getHost(), mongoConfig.getPort())
            mongoClient!!.getDatabase(mongoConfig.getDatabaseName()).getCollection<Recipe>().drop()
        } else {
            throw ExceptionInInitializerError()
        }
    }

    @Test
    fun contextLoads() {
        assertThat(controller).isNotNull();
    }

    private fun insertOneRecipe(recipe: Recipe): String {
        val mongoRecipe = convertToMongo(recipe)
        return mongoRecipe.apply { mongoClient!!.getDatabase("cooking").getCollection<MongoRecipe>().insertOne(mongoRecipe) }._id!!.toString()
    }

    @Test
    fun testTagTarteCarotteOignons() {
        val recipeId: String = insertOneRecipe(tarteCarotteOignons())
        val resultTags: List<String>? = this.restTemplate?.getForObject("http://localhost:$port/recipe/$recipeId/tag", List::class)
        assertThat(resultTags!!)
        assertThat(resultTags).contains(TagEnum.MAIN_COURSE.name)
        assertThat(resultTags).contains(TagEnum.LUNCH.name)
        assertThat(resultTags).contains(TagEnum.DINNER.name)
        assertThat(resultTags).contains(TagEnum.EASY.name)
        assertThat(resultTags).contains(TagEnum.CHEAP.name)
        assertThat(resultTags).contains(TagEnum.QUICK.name)
    }

    private fun getOneRecipe(recipeId: String): Recipe? {
        val mongoRecipe = mongoClient!!.getDatabase("cooking").getCollection<MongoRecipe>().findOne("{_id:ObjectId(\"${recipeId}\")}")
        return convertToDomain(mongoRecipe!!)
    }

}