package com.aott.cooking.search.util

import com.aott.cooking.tag.model.CrawlerEnum
import com.aott.cooking.tag.model.Ingredient
import com.aott.cooking.tag.model.Recipe
import java.util.*

fun tarteCarotteOignons(): Recipe {
    val ingredients = mutableListOf<Ingredient>();
    ingredients.add(Ingredient("pâte brisée", 250, "g"))
    ingredients.add(Ingredient("carotte", 400, "g"))
    ingredients.add(Ingredient("oignon", 3, ""))
    ingredients.add(Ingredient("crème fraîche", 25, "cl"))
    ingredients.add(Ingredient("oeuf", 1, ""))
    ingredients.add(Ingredient("carvi", 1, "cuillère à café"))
    ingredients.add(Ingredient("sel", 0, ""))
    ingredients.add(Ingredient("poivre", 0, ""))
    return Recipe(
            url = "http://www.marmiton.org/recettes/recette_tarte-aux-carottes-et-oignons_21826.aspx",
            updateDate = Date(),
            cookingTime = 20,
            difficulty = 1,
            numberOfPiece = 8,
            cost = 1,
            prepareTime = 20,
            source = CrawlerEnum.MARMITON,
            title = "Tarte aux carottes et oignons",
            totalTime = 40,
            instructions = "empty",
            ingredients = ingredients)
}