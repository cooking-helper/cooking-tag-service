#!/bin/bash

echo "Start mongo service"

/usr/bin/mongod -f /etc/mongod.conf -fork --logpath /var/log/mongod.log --wiredTigerCacheSizeGB 1

echo "run ssh"
/usr/sbin/sshd -D

echo "done"